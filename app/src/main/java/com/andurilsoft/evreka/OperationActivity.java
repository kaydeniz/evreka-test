package com.andurilsoft.evreka;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andurilsoft.evreka.Authentication.MainActivity;
import com.andurilsoft.evreka.Classes.Containers;
import com.andurilsoft.evreka.Classes.PlaceInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class OperationActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "OperationActivity";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final float DEFAULT_ZOOM = 15f;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-40,-168),new LatLng(71,136));


    private boolean mLocationPermissionGranted = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private ImageView gps,edit;
    private GoogleApiClient mGoogleApiClient;
    private Marker mMarker,locMarker;
    private RelativeLayout conInfo,editingInfo,rlEditing,rlChangePosition;
    private Location anlikLoc;
    private ArrayList <Containers> containersList=new ArrayList<>();
    private TextView tvConIdResult,tvFillRateResult,tvTempResult,tvSensorResult,tvDateResult;
    private ProgressBar pbInfo;
    private PlaceInfo mPlace=new PlaceInfo();
    private Button btnYes,btnNo;
    private ProgressDialog pdStart,pdChange;


    Location locationDoc=new Location("loc");


    private static final int ERROR_DIALOG_REQUEST= 9001;

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase database;
    private DatabaseReference myRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operation);

        mAuth=FirebaseAuth.getInstance();
        firebaseUser=mAuth.getCurrentUser();
        database=FirebaseDatabase.getInstance();
        myRef=database.getReference();
        gps=findViewById(R.id.gps);
        edit=findViewById(R.id.edit);

        conInfo=findViewById(R.id.conInfo);
        tvConIdResult=findViewById(R.id.tvConIdResult);
        tvFillRateResult=findViewById(R.id.tvFillRateResult);
        tvTempResult=findViewById(R.id.tvTempResult);
        tvSensorResult=findViewById(R.id.tvSensorResult);
        tvDateResult=findViewById(R.id.tvDateResult);
        pbInfo=findViewById(R.id.pbInfo);
        editingInfo=findViewById(R.id.editingInfo);
        rlEditing=findViewById(R.id.rlEditing);
        btnYes=findViewById(R.id.btnYes);
        btnNo=findViewById(R.id.btnNo);
        rlChangePosition=findViewById(R.id.rlChangePosition);

        pdChange=new ProgressDialog(this);
        pdChange.setMessage("Location is changing.");
        pdChange.setCancelable(false);

        pdStart=new ProgressDialog(this);
        pdStart.setMessage("Map is preparing.");
        pdStart.show();

        isServicesOK();

        getLocationPermission();






    }

    public boolean isServicesOK(){

        int avaliable= GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(OperationActivity.this);

        if(avaliable== ConnectionResult.SUCCESS){


            return true;
        } else if(GoogleApiAvailability.getInstance().isUserResolvableError(avaliable)) {
            // hata çıktı
            Dialog dialog=GoogleApiAvailability.getInstance().getErrorDialog(OperationActivity.this,avaliable,ERROR_DIALOG_REQUEST);
            dialog.show();
        }else {
            Toast.makeText(this,"You can't use map.",Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    private void getLocationPermission(){
        String[] permission={Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(getApplicationContext(),
                FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){

            if(ContextCompat.checkSelfPermission(getApplicationContext(),
                    COARSE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                mLocationPermissionGranted=true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(OperationActivity.this,permission,LOCATION_PERMISSION_REQUEST_CODE);
            }
        }else {
            ActivityCompat.requestPermissions(OperationActivity.this,permission,LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        mLocationPermissionGranted=false;

        switch (requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if(grantResults.length>0){
                    for (int i=0;i<grantResults.length;i++){
                        if(grantResults[i]!=PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionGranted=false;
                            return;
                        }
                    }
                    mLocationPermissionGranted=true;
                    //initiliaze map
                    initMap();
                }
            }
        }
    }

    private void initMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                mMap = googleMap;

                if (mLocationPermissionGranted) {
                    getDeviceLocation();
                    if (ActivityCompat.checkSelfPermission(OperationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(OperationActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setMyLocationButtonEnabled(false);

                    init();
                    getContainerData();

                }
            }
        });
    }

    private void init(){

        mGoogleApiClient=new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();

        gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDeviceLocation();
                conInfo.setVisibility(View.INVISIBLE);
                rlChangePosition.setVisibility(View.INVISIBLE);
            }
        });

    }

    private void getDeviceLocation() {

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {

            if (mLocationPermissionGranted) {

                final Task location = mFusedLocationProviderClient.getLastLocation();



                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Location currentLocation = (Location) task.getResult();

                            if (location != null) {
                                anlikLoc = new Location(currentLocation);


                                try {
                                    moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                            DEFAULT_ZOOM, "My Location");

                                } catch (Exception e) {

                                }

                            }

                        } else {
                            Toast.makeText(OperationActivity.this, "Location is not found.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            }

        } catch (SecurityException e) {

        }

    }

    private void moveCamera(LatLng latLng, float zoom, String title){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        int height = 75;
        int width = 50;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.markeruser);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        if (locMarker == null) {
            Marker mar = mMap.addMarker(new MarkerOptions().position(latLng)
                    .title(title).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
            locMarker=mar;
        }

    }

    private void moveCameraToChanging(LatLng latLng, float zoom, String title){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        int height = 75;
        int width = 50;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.blueicon);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        if(mMarker==null) {
            Marker markerNew = mMap.addMarker(new MarkerOptions().position(latLng)
                    .title(title).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
            mMarker=markerNew;
        } else {
            mMarker.remove();
            Marker markerNew = mMap.addMarker(new MarkerOptions().position(latLng)
                    .title(title).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
            mMarker=markerNew;
        }

    }

    private void getContainerData() {

        myRef=FirebaseDatabase.getInstance().getReference().child("containers");

        myRef.getRef().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for(DataSnapshot ds: dataSnapshot.getChildren()){

                    Containers con=ds.getValue(Containers.class);
                    containersList.add(con);
                }

                showOnMap(containersList);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });






    }

    private void showOnMap(ArrayList<Containers> list) {

        for (int i=0;i<list.size();i++) {

            int height = 75;
            int width = 50;
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.mapicon);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

            double lat= Double.valueOf(list.get(i).getLatitude()) ;
            double lon= Double.valueOf(list.get(i).getLongitude());

            LatLng latLng=new LatLng(lat,lon);


            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .snippet("Container occupancy rate: "+containersList.get(i).getFillRate()+"%")
                    .title(containersList.get(i).getConId());
            options.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
            mMap.addMarker(options);

            editingInfo.setVisibility(View.INVISIBLE);

            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    conInfo.setVisibility(View.INVISIBLE);
                    editingInfo.setVisibility(View.INVISIBLE);
                    Log.d("lokasyon:",String.valueOf(latLng.latitude));
                }
            });

            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    pbInfo.setVisibility(View.VISIBLE);
                    editingInfo.setVisibility(View.INVISIBLE);
                    openDialog(marker.getTitle());

                }
            });

        }

        pdStart.dismiss();
        Toast.makeText(this,"Map is ready.",Toast.LENGTH_SHORT).show();

    }

    private void openDialog(String id) {

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

        Query query = reference.child("containers").orderByChild("conId").equalTo(id);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        Containers container=new Containers();
                        container=issue.getValue(Containers.class);

                        conInfo.setVisibility(View.VISIBLE);

                        tvConIdResult.setText(container.getConId());
                        tvFillRateResult.setText(container.getFillRate()+"%");
                        tvTempResult.setText(container.getTemp()+"°C");
                        tvSensorResult.setText(container.getSensorId());
                        tvDateResult.setText(container.getLastUpdate());
                        pbInfo.setVisibility(View.INVISIBLE);

                        final Containers finalContainer = container;
                        rlEditing.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                editingInfo.setVisibility(View.VISIBLE);
                                conInfo.setVisibility(View.INVISIBLE);

                                //position change logic

                                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                                    @Override
                                    public void onMapClick(final LatLng latLng) {

                                        moveCameraToChanging(latLng,DEFAULT_ZOOM,"New Location of Container");
                                        rlChangePosition.setVisibility(View.VISIBLE);


                                        btnYes.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                pdChange.show();
                                                Map<String, Object> postValues = new HashMap<String,Object>();
                                                postValues.put("latitude",String.valueOf(latLng.latitude));
                                                postValues.put("longitude",String.valueOf(latLng.longitude));
                                                myRef.child(finalContainer.getConId()).updateChildren(postValues);
                                                mMarker.remove();
                                                rlChangePosition.setVisibility(View.INVISIBLE);
                                                mMap.clear();
                                                pdChange.dismiss();
                                                getContainerData();
                                                Toast.makeText(OperationActivity.this, "Position changing is successfull.", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                                        btnNo.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                rlChangePosition.setVisibility(View.INVISIBLE);
                                                mMarker.remove();
                                                getContainerData();

                                            }
                                        });

                                    }
                                });

                            }
                        });


                    }



                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }



    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_signout:
                alert();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void alert() {
        new AlertDialog.Builder(this).setIcon(R.drawable.ic_warning_black_24dp).setTitle("Exit")
                .setMessage("Are you sure to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAuth.signOut();
                        Intent intent = new Intent(OperationActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }).setNegativeButton("No", null).show();
    }
}
