package com.andurilsoft.evreka.Authentication;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.andurilsoft.evreka.Classes.Containers;
import com.andurilsoft.evreka.OperationActivity;
import com.andurilsoft.evreka.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class LoginFragment extends Fragment {
    private static final String TAG = "LoginFragment";

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private EditText etLoginUsername, etLoginPassword;
    private Button btnLogin;
    private ScrollView scroll;
    private ProgressBar pbLogin;
    private String email="";
    private String pass="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login,container,false);

        mAuth=FirebaseAuth.getInstance();
        firebaseUser=mAuth.getCurrentUser();
        database=FirebaseDatabase.getInstance();
        myRef=database.getReference();

        etLoginPassword= view.findViewById(R.id.etLoginPass);
        etLoginUsername=view.findViewById(R.id.etLoginUsername);
        btnLogin=view.findViewById(R.id.btnLogin);
        scroll=view.findViewById(R.id.scroll);
        pbLogin=view.findViewById(R.id.pbLogin);

        if(checkInternetConnection()){

            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    email = etLoginUsername.getText().toString().trim();
                    pass = etLoginPassword.getText().toString().trim();

                    if(email.isEmpty() || pass.isEmpty()){

                        Toast.makeText(getContext(),"Please fill empty blanks!",Toast.LENGTH_SHORT).show();

                    }else{

                        try {
                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                        } catch (Exception e) {
                        }
                        pbLogin.setVisibility(View.VISIBLE);

                        if(checkInternetConnection()) {
                            loginFunc();
                        } else {
                            Toast.makeText(getContext(),"You have no internet connection.",Toast.LENGTH_SHORT).show();
                        }


                    }
                }
            });

        } else {

            Toast.makeText(getContext(),"You have no internet connection.",Toast.LENGTH_SHORT).show();
        }



        return view;
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void loginFunc() {

        mAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(getActivity(),
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){

                            if(mAuth.getCurrentUser().isEmailVerified()){

                                pbLogin.setVisibility(View.INVISIBLE);

                                Intent in = new Intent(getContext(), OperationActivity.class);
                                startActivity(in);

                            } else{
                                pbLogin.setVisibility(View.INVISIBLE);
                                Toast.makeText(getContext(),"Please verify your e-mail address.",Toast.LENGTH_SHORT).show();
                            }

                        }
                        else{

                            Toast.makeText(getContext(),task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                            pbLogin.setVisibility(View.INVISIBLE);


                        }
                    }

                });
    }

    private void randomDatabase () {

        //For setup database

        String id="";

        for (int i=1;i<1001;i++){

            double lat=39.824448;
            double lon=32.658143;

            int number= (int)(Math.random()*1000000) ;
            double willAdd= ((double) number)/5000000;

            int number2= (int)(Math.random()*1000000) ;
            double willAdd2= ((double) number2)/5000000;

            if( 0<i && i<10) {
                id="000"+i;
            } else if(9<i && i<100){
                id="00"+i;
            } else if(99<i && i<1000){
                id="0"+i;
            } else {
                id=""+i;
            }
            Log.d("idler",id);

            int whiichChange= 2;
            Log.d("evrekam which cahange",String.valueOf(whiichChange));

            if(whiichChange==0){
                lat=lat+willAdd;
            } else if(whiichChange==1){
                lon=lon+willAdd;
            }else {
                lat=lat+willAdd;
                lon=lon+willAdd2;
            }

            int fillRate= (int)(Math.random()*100);
            int temp= (int)(Math.random()*35);

            Date date = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
            String lastUpdate=sdf.format(date);

            String sensorId= "s"+id;

            Containers container=new Containers();
            container.setConId(id);
            container.setFillRate(String.valueOf(fillRate));
            container.setLastUpdate(String.valueOf(lastUpdate));
            container.setLatitude(String.valueOf(lat));
            container.setLongitude(String.valueOf(lon));
            container.setTemp(String.valueOf(temp));
            container.setSensorId(sensorId);

            Log.d("veritabanı",container.getConId());
            Log.d("veritabanı",container.getFillRate());
            Log.d("veritabanı",container.getLastUpdate());
            Log.d("veritabanı",container.getLatitude());
            Log.d("veritabanı",container.getLongitude());
            Log.d("veritabanı",container.getTemp());



            myRef.child("containers")
                    .child(id)
                    .setValue(container).addOnCompleteListener(new OnCompleteListener<Void>() {

                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                        }
                    }
                    );

        }


    }

}
