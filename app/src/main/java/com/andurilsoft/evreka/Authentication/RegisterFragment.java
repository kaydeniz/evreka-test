package com.andurilsoft.evreka.Authentication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.andurilsoft.evreka.Classes.Users;
import com.andurilsoft.evreka.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static android.content.Context.INPUT_METHOD_SERVICE;


public class RegisterFragment extends Fragment {
    private static final String TAG = "RegisterFragment";

    private EditText etRegUsername, etRegPass,etRegPassAgain;
    private Button btnRegister;
    private ProgressBar pbReg;

    private FirebaseAuth mAuth;
    private FirebaseUser firebaseUser;
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    private Users user=new Users();

    private String email,pass,passAgain;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register,container,false);

        etRegUsername= view.findViewById(R.id.etRegUsername);
        etRegPass= view.findViewById(R.id.etRegPass);
        etRegPassAgain= view.findViewById(R.id.etRegPassAgain);
        btnRegister=view.findViewById(R.id.btnRegister);
        pbReg=view.findViewById(R.id.pbReg);

        mAuth=FirebaseAuth.getInstance();
        firebaseUser=mAuth.getCurrentUser();
        database=FirebaseDatabase.getInstance();
        myRef=database.getReference();


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                email= etRegUsername.getText().toString().trim();
                pass= etRegPass.getText().toString().trim();
                passAgain=etRegPassAgain.getText().toString().trim();


                if(email.isEmpty()){
                    etRegUsername.setError("Please enter e-mail");
                    etRegUsername.requestFocus();
                    return;
                }

                if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    etRegUsername.setError("Please enter e-mail in true format");
                    etRegUsername.requestFocus();
                    return;
                }

                if(pass.isEmpty()){
                    etRegPass.setError("Please enter password");
                    etRegPass.requestFocus();
                    return;
                }

                if(passAgain.isEmpty()){
                    etRegPassAgain.setError("Please enter password again");
                    etRegPassAgain.requestFocus();
                    return;
                }

                if(pass.length()<6){
                    etRegPass.setError("Your password must be equal or more than 6 characters");
                    etRegPass.requestFocus();
                    return;
                }



                    if(pass.equals(passAgain)) {
                        try {
                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                        } catch (Exception e) {
                        }
                        pbReg.setVisibility(View.VISIBLE);
                        registerFunc();
                    }else {
                        Toast.makeText(getContext(),"Entering passwords are not equal each other.",Toast.LENGTH_SHORT).show();
                    }
            }
        });


        return view;
    }

    private void registerFunc() {

        mAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(Task<AuthResult> task) {
                if(task.isSuccessful()){

                    pbReg.setVisibility(View.INVISIBLE);

                    emailVerif();

                    user.setEmail(email);
                    user.setUserId(mAuth.getCurrentUser().getUid());


                    myRef
                            .child("users")
                            .child(mAuth.getCurrentUser().getUid())
                            .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){

                                Toast.makeText(getContext(),"Registeration is successfull, please chech your e-mail addresss.",Toast.LENGTH_SHORT).show();

                                mAuth.signOut();



                            }
                        }
                    });

                }
                else{
                    Toast.makeText(getContext(),task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                    pbReg.setVisibility(View.INVISIBLE);

                }

            }
        });

    }

    private void emailVerif (){


        if(mAuth.getCurrentUser()!=null){
            mAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(getActivity(),new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){

                    } else {
                        Toast.makeText(getContext(),"Sorry, there is a problem. Try again.",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

}
